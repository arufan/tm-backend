"user strict";
var connection =require('../dbConnection');

class orderController{

   // const entity='_order';

   save(req,res){       
        console.log(req.body);
        var data=(req.body);        
        let qry='insert into _order SET ?';
        connection.query(qry,data,function(err,rows){
            if(err){
                console.log(err);
                res.send('respond with a resource');
            } else{
            res.send(rows);
            }  
        });
   }

   update(req,res){       
    let id=req.params.id; 
    var data=(req.body);        
    let qry=`update _order SET ? where id = ${id}`;
    connection.query(qry,data,function(err,rows){
        if(err){
            console.log(err);
            res.send('respond with a resource');
        } else{
        res.send(rows);
        }  
    });
}

   get(req,res){
    connection.query(`select * from _order where id = ${req.params.id}`,function(err,rows){
        if(err){
            console.log(err);
            res.send('respond with a resource');
        } else{
            
            res.send(rows[0]);
        }  
    });
   }

   list(req, res){
        connection.query('select * from _order',function(err,rows){
              if(err){
                  console.log(err);
                  res.send('respond with a resource');
              } else{
                res.send(rows);
              }  
         });
    }


    delete(req, res){
        let id=req.params.id; 
        connection.query(`delete from _order where id = ${id}`,function(err,rows){
              if(err){
                  console.log(err);
                  res.send('respond with a resource');
              } else{
                res.send(rows);
              }  
         });
    }
}




module.exports=orderController;