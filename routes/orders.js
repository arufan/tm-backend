"user strict";

var express = require('express');
var router = express.Router();

var OrderController=require('../controllers/orderCtr');

const orderObj=new OrderController();

router.get('/list',orderObj.list );
router.get('/:id',orderObj.get );
router.post('/',orderObj.save );
router.put('/:id',orderObj.update );
router.delete('/:id',orderObj.delete );

module.exports = router;
